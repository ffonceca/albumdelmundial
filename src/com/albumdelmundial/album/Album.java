package com.albumdelmundial.album;

import com.albumdelmundial.figurita.FiguritaTradicional;
import com.albumdelmundial.participante.Participante;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public abstract class Album {

    private Integer codigo;
    private HashMap<Integer, String> paises;
    public HashMap<Integer, FiguritaTradicional> figuritasTradicionales;
    private Album album;
    private String premio;

    public Album (String tipoAlbum, String[] paises, Map<String, Integer> ranking) {
        this.codigo = getRandomCode();
        this.paises = generarPaises(paises);
        this.premio = obtenerPremioSegunAlbum(tipoAlbum);
        this.album  = obtenerAlbumSegunTipo(tipoAlbum);
        this.figuritasTradicionales = obtenerFiguritas(paises, ranking);
    }

    public Album() {

    }

    private HashMap<Integer,FiguritaTradicional> obtenerFiguritas(String[] paises, Map<String, Integer> ranking) {

        HashMap<Integer,FiguritaTradicional> figuritas = new HashMap<>();

        for (int x=0; x < paises.length; x++){
            figuritas.put(x, new FiguritaTradicional(paises[x], getRandomPlayerByCountry(paises[x]),0, ranking.get(paises[x])));
        }

        return figuritas;
    }

    private String getRandomPlayerByCountry(String pais) {
        return "messi";
    }

    private Album obtenerAlbumSegunTipo(String tipoAlbum) {

        switch (tipoAlbum) {
            case "EXTENDIDO":
                return new AlbumExtendido();
            case "WEB":
                return new AlbumWeb();
            default:
                return new AlbumTradicional();
        }

    }

    private String obtenerPremioSegunAlbum(String tp) {

        switch (tp){
            case "TRADICIONAL":
                return "Pelota";

            case "EXTENDIDO":
                return "Pelota y Viaje";
            case "WEB":
                return "CODIGO-EXTRA-4-FIGURITAS";
            default:
                return "Pelota";
        }

    }

    private Integer getRandomCode() {
        Random random = new Random();

        return random.nextInt();
    }

    private HashMap<Integer, String> generarPaises(String[] paises) {

        return null;
    }

    public Boolean estaCompleto(Participante participante){
        return this.figuritasTradicionales.equals(participante.getFiguritas());
    }

    public Boolean pegarFigurita(FiguritaTradicional figurita){
        return false;
    }

}
