package com.albumdelmundial.participante;

import java.util.HashMap;
import java.util.Map;

import com.albumdelmundial.album.Album;
import com.albumdelmundial.figurita.FiguritaTradicional;

public class Participante {
	private int dni;
	private String nombre;
	private Album album;
	private HashMap<Integer, FiguritaTradicional> figuritas;
	private HashMap<Integer, FiguritaTradicional> repetidas;
	private String[] premio; //dos valores: {boolean - premio}
	
	public boolean completoAlbum() {
		return mismaCantidadFigusConAlbum();
	}
	
	public boolean tieneSinPegar() { //Si al menos una figu en figuritas no se pegó;
		
	}
	
	public boolean tieneRepetidas() {
		return repetidas.size() > 0;
	}
	
	public boolean recibioPremio() {
		return Boolean.parseBoolean(premio[0]);
	}
	
	private boolean mismaCantidadFigusConAlbum() { //Comparo la cantidad de figuritas del jugador con las del album
		HashMap<Integer, FiguritaTradicional> figuritasDeAlbum = album.figuritasTradicionales;
		
		if (figuritasDeAlbum.isEmpty()) return false;
		
		return figuritas.equals(figuritasDeAlbum);
	}
	
	public int getDni() {
		return dni;
	}
	public void setDni(int dni) {
		this.dni = dni;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Album getAlbum() {
		return album;
	}
	public void setAlbum(Album album) {
		this.album = album;
	}
	public HashMap<Integer, FiguritaTradicional> getFiguritas() {
		return figuritas;
	}
	public void setFiguritas(FiguritaTradicional figuritas) {
		this.figuritas = figuritas;
	}
	public HashMap<Integer, FiguritaTradicional> getRepetidas() {
		return repetidas;
	}
	public void setRepetidas(FiguritaTradicional repetidas) {
		this.repetidas = repetidas;
	}
	public String getPremio() {
		return premio;
	}
	public void setPremio(String premio) {
		this.premio = premio;
	}
	
}
