package com.albumdelmundial;

import java.util.List;

public class AlbumDelMundial implements IAlbumDelMundial {

    @Override
    public int registrarParticipante(int dni, String nombre, String tipoAlbum) {
        return 0;
    }

    @Override
    public void comprarFiguritas(int dni) {

    }

    @Override
    public void comprarFiguritasTop10(int dni) {

    }

    @Override
    public void comprarFiguritasConCodigoPromocional(int dni) {

    }

    @Override
    public List<String> pegarFiguritas(int dni) {
        return null;
    }

    @Override
    public boolean llenoAlbum(int dni) {
        return false;
    }

    @Override
    public String aplicarSorteoInstantaneo(int dni) {
        return null;
    }

    @Override
    public int buscarFiguritaRepetida(int dni) {
        return 0;
    }

    @Override
    public boolean intercambiar(int dni, int codFigurita) {
        return false;
    }

    @Override
    public boolean intercambiarUnaFiguritaRepetida(int dni) {
        return false;
    }

    @Override
    public String darNombre(int dni) {
        return null;
    }

    @Override
    public String darPremio(int dni) {
        return null;
    }

    @Override
    public String listadoDeGanadores() {
        return null;
    }

    @Override
    public List<String> participantesQueCompletaronElPais(String nombrePais) {
        return null;
    }
}
