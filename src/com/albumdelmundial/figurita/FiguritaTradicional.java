package com.albumdelmundial.figurita;

public class FiguritaTradicional {

    private String pais;
    private String jugador;
    private double valorBase;
    private Integer rankingPais;

    public FiguritaTradicional(String pais, String jugador, double valorBase, Integer rankingPais) {
        this.jugador = jugador;
        this.pais = pais;
        this.valorBase = valorBase;
        this.rankingPais = rankingPais;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getJugador() {
        return jugador;
    }

    public void setJugador(String jgador) {
        this.jugador = jgador;
    }

    public double getValorBase() {
        return valorBase;
    }

    public void setValorBase(double valorBase) {
        this.valorBase = valorBase;
    }

    public Integer getRankingPais() {
        return rankingPais;
    }

    public void setRankingPais(Integer rankingPais) {
        this.rankingPais = rankingPais;
    }

}
