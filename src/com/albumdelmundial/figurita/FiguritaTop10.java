package com.albumdelmundial.figurita;

public class FiguritaTop10 extends FiguritaTradicional{
	
	private int año;
	private String balon;

	public FiguritaTop10(String pais, double valorBase, Integer rankingPais) {
		super(pais, valorBase, rankingPais);
	}

	public String tipoDeBalon() {
		return this.balon;
	}
	
	public double calcularValorFinal() {
		return 0;
	}
}
